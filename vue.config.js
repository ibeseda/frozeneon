module.exports= {
    publicPath: './',
    outputDir: 'build',
    assetsDir: 'min',
    productionSourceMap: false,

    transpileDependencies: ['vuetify']
}
